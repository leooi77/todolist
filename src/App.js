import React from 'react';
import Login from './components/Login/Login';
import Register from './components/Register/Register';
import Tasks from './components/Tasks/Tasks';

import { Route, Switch, BrowserRouter as Router } from 'react-router-dom'
import { Provider } from 'react-redux';
import { store } from './data/redux/store'
import './css/styles.scss';

import LoaderSpinner from './components/LoaderSpinner/LoaderSpinner'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
function App() {
  toast.configure({
    autoClose: 2000,
    draggable: false,
    //etc you get the idea
  })
  const routing = (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/tasks" component={Tasks} />
          <Route component={Login} />
        </Switch>
      </Router>
      <LoaderSpinner />
    </Provider>
  )
  return (
    routing
  );
}

export default App;
