// import firebase from "firebase"
import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';
const config = {
    apiKey: 'AIzaSyCGN6WoDBeHl8dm-Y36Ur0PpE3kofXWwRA',
    authDomain: '',
    databaseURL: 'https://to-do-app-b26b3.firebaseio.com/',
    projectId: 'to-do-app-b26b3'

}
console.log('init firebase')
firebase.initializeApp(config)
// firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL).then(function () {
//     console.log('success')
// })
//     .catch(function (error) {
//         // Handle Errors here.
//         var errorCode = error.code;
//         var errorMessage = error.message;
//         console.log('errorCode', errorCode)
//     });
const databaseRef = firebase.database().ref();


export const dbRef = databaseRef.child("todos");
