import { CONNECTING, SIGN_IN, SIGN_OUT, REGISTER, GET_RECORDS } from '../actions/types';

const initialState = {
    records: {},
    user: { email: '' },
    connecting: false
}
export default function recordsReducer(state = initialState, action) {
    switch (action.type) {
        case CONNECTING:
            return {
                ...state,
                connecting: action.data
            }
        case GET_RECORDS:
            let data = {}
            for (let i in action.data) {
                if (action.data[i].user === state.user.email) {
                    data[i] = action.data[i]
                }
            }
            return {
                ...state,
                records: data
            }
        case SIGN_IN:
            return {
                ...state,
                user: { email: action.data.email },
                logged: true
            }
        case SIGN_OUT:
            return {
                ...initialState
            }
        case REGISTER:
            return {
                ...state,
                logged: false
            }
        default:
            return state;
    }
}