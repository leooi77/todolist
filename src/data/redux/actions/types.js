export const SIGN_IN = 'SIGN_IN';
export const SIGN_OUT = 'SIGN_OUT';
export const REGISTER = 'REGISTER';
export const GET_RECORDS = 'GET_RECORDS'
export const ADD_RECORD = 'ADD_RECORD';
export const CONNECTING = "CONNECTING"