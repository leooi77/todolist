import { CONNECTING, SIGN_IN, SIGN_OUT, REGISTER, GET_RECORDS } from './types';
import { dbRef } from '../../firebase'
import { store } from "../store"
import firebase from "firebase/app"
import 'firebase/auth';
import { toast } from 'react-toastify';

export const sign_in = (data) => {
    return {
        type: SIGN_IN,
        data
    }
};
export const sign_out = () => {
    return {
        type: SIGN_OUT
    }
};
export const register = (data) => {
    return {
        type: REGISTER,
        data
    }
};
export const get_records = (data) => {
    return {
        type: GET_RECORDS,
        data
    }
};
export const connecting = (data) => {
    return {
        type: CONNECTING,
        data
    }
}

export const thunk_signIn = (data) => {

    return async (dispatch) => {
        dispatch(connecting(true))
        await firebase.auth().signInWithEmailAndPassword(data.email, data.password).then((response) => {
            dispatch(connecting(false))
            dispatch(sign_in({ email: response.user.email }))
        }).catch(function (error) {
            dispatch(connecting(false))
            toast.error(error.message, {
                position: toast.POSITION.TOP_LEFT
            });
            return
        });

    }
};
export const thunk_signOut = () => {
    return async (dispatch) => {
        dispatch(connecting(true))
        await firebase.auth().signOut().then((response) => {
            dispatch(connecting(false))
            dispatch(sign_out())
        }).catch((error) => {
            dispatch(connecting(false))
            dispatch(sign_out())
            if (error.message) {
                toast.error(error.message, {
                    position: toast.POSITION.TOP_LEFT
                });
            }
            return
        });

    }
}
export const thunk_register = (data) => {
    return async (dispatch) => {
        dispatch(connecting(true))
        await firebase.auth().createUserWithEmailAndPassword(data.email, data.password).then((response) => {
            dispatch(connecting(false))
            console.log(response)
            toast.success("Register success!", {
                position: toast.POSITION.TOP_LEFT
            });
        }).catch((error) => {
            dispatch(connecting(false))
            if (error.message) {
                toast.error(error.message, {
                    position: toast.POSITION.TOP_LEFT
                });
            }
            return
        });
    }
}
export const thunk_add_record = (data) => {
    return async (dispatch) => {
        let newRecord = {
            user: store.getState().data.user.email,
            description: data.description,
            completed: false,
            important: false
        }
        console.log("Saving Data", newRecord)
        dispatch(connecting(true))
        dbRef.push(newRecord, (error) => {
            if (error) {
                dispatch(connecting(false))
                console.log("Save Data Error", error)
            } else {
            }
        });
    }
    //dispatch(add_record())
}
export const thunk_delete_record = (key) => {
    return async (dispatch) => {
        dispatch(connecting(true))
        dbRef.child(key).remove().then(() => {
            console.log('remove success')
        }).catch((err) => { console.log('remove error', err) });
    }
}
export const thunk_edit_record = (key, data) => {
    return async (dispatch) => {
        dispatch(connecting(true))
        dbRef.child(key).set(data).then(() => {
            console.log('edit success')
        }).catch((err) => { console.log('edit error', err) });
    }
}