import React, { Component } from 'react';
import '../../css/styles.scss';
class InputText extends Component {
    render() {
        return (
            <input
                className={`input ${this.props.className}`}
                autoComplete="off"
                onChange={this.props.onChange}
                value={this.props.value}
                type={this.props.type}
                placeholder={this.props.placeholder}
            />
        )

    }
}


export default InputText