import React, { Component } from 'react';
import '../../css/styles.scss';
import { connect } from "react-redux";
import { thunk_register } from '../../data/redux/actions/index'
import { toast } from 'react-toastify';
import InputText from "../InputText/InputText"
import ToDoLogo from "../ToDoLogo/ToDoLogo"
class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: ''
        }
    }
    componentDidMount() {
    }
    register = async () => {
        if (!this.state.email || !this.state.password) {
            toast.error("Please enter your email and password.", {
                position: toast.POSITION.TOP_LEFT
            });
            return
        }
        await this.props.thunk_register({ email: this.state.email, password: this.state.password })
    }
    goLogin = () => {
        this.props.history.push('/login')
    }
    render() {
        return (

            <div className='loginContainer'>
                <ToDoLogo />
                <div className="loginContainer__box">
                    <InputText className="input--bottomBorder" onChange={(event) => { this.setState({ email: event.target.value }) }} value={this.state.email} type="text" placeholder='email' />
                    <InputText className="input--bottomBorder" onChange={(event) => { this.setState({ password: event.target.value }) }} value={this.state.password} type="password" placeholder='password' />

                    <div className='button button--active' onClick={this.register}>register</div>
                    <div style={{ height: '10px' }} />
                    <div className="button--active" style={{ textAlign: 'center' }} onClick={this.goLogin}>back to login</div>
                </div>
            </div>

        )
    }
}
const mapStateToProps = state => {
    return { data: state.data };
};
const mapDispatchToProps = (dispatch) => {
    return {
        thunk_register: data => dispatch(thunk_register(data))
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Register);
