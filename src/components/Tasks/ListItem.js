import React, { Component } from 'react';
import '../../css/styles.scss'
import { connect } from "react-redux";
import { thunk_delete_record, thunk_edit_record } from "../../data/redux/actions/index"
import Delete from "@material-ui/icons/DeleteOutlined"
import Done from "@material-ui/icons/Done"
import Priority from "@material-ui/icons/PriorityHigh"
class ListItem extends Component {
    deleteRecord = () => {
        if (this.props.task && this.props.task.key) {
            this.props.thunk_delete_record(this.props.task.key)
        }
    }
    editRecord = (type) => {
        if (type === 'important') {
            this.props.thunk_edit_record(this.props.task.key, { ...this.props.task.data, important: !this.props.task.data.important })
        } else if (type === 'completed') {
            this.props.thunk_edit_record(this.props.task.key, { ...this.props.task.data, completed: !this.props.task.data.completed })
        }
    }
    render() {
        return (


            <div className='listItem'>
                <div className={`listItem__desc ${this.props.task.data.completed ? "listItem--lineThrough" : ""}`}>{this.props.task.data.description}</div>
                <Priority onClick={() => { this.editRecord('important') }} className={`listItem__icons ${this.props.task.data.important ? "listItem--priority" : ""}`} />
                <Done onClick={() => { this.editRecord('completed') }} className={`listItem__icons ${this.props.task.data.completed ? "listItem--completed" : ""}`} />
                <Delete className="listItem__icons" onClick={this.deleteRecord} />
            </div>


        )

    }

}
const mapStateToProps = state => {
    return { redux: state.data };
};
const mapDispatchToProps = (dispatch) => {
    return {
        thunk_delete_record: (data) => dispatch(thunk_delete_record(data)),
        thunk_edit_record: (key, data) => dispatch(thunk_edit_record(key, data)),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(ListItem);


