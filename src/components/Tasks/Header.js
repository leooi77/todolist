import React, { Component } from 'react';
import '../../css/styles.scss';
import ExitToApp from "@material-ui/icons/ExitToApp"
import { connect } from "react-redux";
import { thunk_add_record, thunk_signOut } from "../../data/redux/actions/index"
import InputText from "../InputText/InputText"
import { toast } from 'react-toastify';
import Add from "@material-ui/icons/AddCircleOutline"
import ToDoLogo from "../ToDoLogo/ToDoLogo"
class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {
            newTask: ''
        }
    }
    goBack = async () => {
        if (this.props.redux.connecting) {
            return
        }
        await this.props.thunk_signOut()
        this.props.history.replace('/login')
    }
    addData = () => {
        if (this.props.redux.connecting) {
            return
        }
        if (this.state.newTask === "") {
            toast.warn("Please enter a new task.", {
                position: toast.POSITION.TOP_LEFT
            });
            return
        }
        this.props.thunk_add_record({ description: this.state.newTask })
        this.setState({ newTask: '' })
    }
    updateTaskText = (event) => {
        this.setState({ newTask: event.currentTarget.value })
    }
    render() {
        return (
            <>
                <div className='header'>
                    <div className='header__row'>
                        <div></div>
                        <ToDoLogo />
                        <ExitToApp onClick={this.goBack} style={{ fontSize: '30px' }} />
                    </div>
                </div>
                <div className='header__row header--add'>
                    <InputText onChange={this.updateTaskText} value={this.state.newTask} className="input--bottomBorder" placeholder='enter a new task' />
                    <div style={{ width: '10px' }} />
                    <Add style={{ fontSize: '30px' }} className={`${this.props.redux.connecting ? "button--inactive" : "button--active"}`} onClick={this.addData} />
                    {/* <div className={`button ${this.props.redux.connecting ? "button--inactive" : "button--active"}`} onClick={this.addData}><Add /></div> */}
                </div>
            </>

        )

    }

}

const mapStateToProps = state => {
    return { redux: state.data };
};
const mapDispatchToProps = (dispatch) => {
    return {
        thunk_add_record: data => dispatch(thunk_add_record(data)),
        thunk_signOut: data => dispatch(thunk_signOut())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
