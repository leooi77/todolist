import React, { Component } from 'react';
import '../../css/styles.scss';

import { connect } from "react-redux";
import ListItem from './ListItem';
import Header from './Header'
import { get_records, connecting } from "../../data/redux/actions/index"
import { dbRef } from "../../data/firebase";
class Tasks extends Component {

    componentDidMount() {
        this.props.connecting(true)
        if (this.props.redux.user.email) {
            dbRef.on("value", (snapshot) => {
                console.log("records", snapshot.val());
                this.props.connecting(false)
                this.props.get_records(snapshot.val())
            });

        } else {
            this.props.connecting(false)
            this.goBack()
        }

    }
    componentWillUnmount() {
        dbRef.off("value")
    }
    goBack = () => {
        this.props.history.replace('/login')
    }
    generateTasks = () => {
        const tasks = Object.keys(this.props.redux.records).map((key, index) => {
            return <ListItem task={{
                key: key, data: this.props.redux.records[key]
            }} key={'item' + index} />
        });
        console.log('tasks', tasks)
        if (tasks.length <= 0) {
            return <div className="listItem__noRecord">no records found</div>
        }
        return tasks
    }
    render() {
        return (
            <>
                <Header history={this.props.history} />

                <div className="taskList">
                    {this.generateTasks()}
                </div>
            </>
        )

    }

}
const mapStateToProps = state => {
    return { redux: state.data };
};
const mapDispatchToProps = (dispatch) => {
    return {
        get_records: (data) => dispatch(get_records(data)),
        connecting: flag => dispatch(connecting(flag))
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Tasks);
