import React, { Component } from 'react';
import Loader from 'react-loader-spinner'
import { connect } from "react-redux";
import '../../css/styles.scss';
class LoaderSpinner extends Component {
    render() {
        return (
            this.props.data.connecting ?
                <div className="spinnerLoader" >
                    <Loader type="MutatingDots"
                        color="#00ADB5"
                        height={80}
                        width={80} />

                </div> : null
        )

    }
}
const mapStateToProps = state => {
    return { data: state.data };
};

export default connect(mapStateToProps, null)(LoaderSpinner);