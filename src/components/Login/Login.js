import React, { Component } from 'react';
import '../../css/styles.scss';
import { connect } from "react-redux";
import { thunk_signIn, sign_in } from '../../data/redux/actions/index'
import firebase from 'firebase/app';
import 'firebase/auth';
import InputText from "../InputText/InputText"
import { toast } from 'react-toastify';
import ToDoLogo from "../ToDoLogo/ToDoLogo"
class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: ''
        }
    }
    componentDidMount() {
        let user = firebase.auth().currentUser
        console.log('currentUser', user)
        if (user) {
            this.props.sign_in({ email: user.email })
        }
    }
    login = async () => {
        if (!this.state.email || !this.state.password) {
            toast.error("Please enter your email and password.", {
                position: toast.POSITION.TOP_LEFT
            });
            return
        }
        await this.props.thunk_signIn({ email: this.state.email, password: this.state.password })
        if (this.props.data.user.email) {
            this.props.history.replace('/tasks')
        }
        //t
    }
    goRegister = () => {
        this.props.history.push('/register')
    }
    render() {
        return (

            <div className='loginContainer'>
                <ToDoLogo />
                <div className="loginContainer__box">
                    <InputText className='input--bottomBorder' onChange={(event) => { this.setState({ email: event.target.value }) }} value={this.state.email} type="text" placeholder='email' />
                    <InputText className='input--bottomBorder' onChange={(event) => { this.setState({ password: event.target.value }) }} value={this.state.password} type="password" placeholder='password' />
                    <div className='button button--active' onClick={this.login}>login</div>
                    <div style={{ height: '10px' }} />
                    <div className="button--active" style={{ textAlign: 'center' }} onClick={this.goRegister}>don't have an account? sing up now!</div>
                </div>
            </div>

        )
    }
}
const mapStateToProps = state => {
    return { data: state.data };
};
const mapDispatchToProps = (dispatch) => {
    return {
        thunk_signIn: data => dispatch(thunk_signIn(data)),
        sign_in: data => dispatch(sign_in(data))
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);
